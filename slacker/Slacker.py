import os
import json

import requests


class Slacker:
    def __init__(self, channel_url: str, message: str):
        self.channel_url = os.environ.get("SLACK_POST_URL", channel_url)
        self.message = message

    def write_to_channel(self) -> None:
        msg = {"text": self.message}
        post_res = requests.post(self.channel_url, data=json.dumps(msg))
        post_res.raise_for_status()
