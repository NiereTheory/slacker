import argparse

from slacker import Slacker


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--channel-url", type=str, required=False, dest="channel_url")
    parser.add_argument("--message", type=str, required=True, dest="message")
    args = vars(parser.parse_args())
    s = Slacker(**args)
    s.write_to_channel()


if __name__ == "__main__":
    main()
