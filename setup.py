from setuptools import setup
import os

requirements = []
if os.path.isfile("requirements.txt"):
    with open("requirements.txt") as f:
        for line in f.readlines():
            if not line.startswith("#"):
                requirements.append(line.strip())

print("in setup", requirements)

setup(
    name="slacker",
    version="0.0.1",
    description="My private package from private github repo",
    url="https://gitlab.com/NiereTheory/slacker.git",
    author="Ben Niere",
    license="unlicense",
    packages=["slacker"],
    zip_safe=False,
    install_requires=requirements,
    include_package_data=True,
    entry_points={"console_scripts": ["slacker=slacker.cli:main"]},
)
